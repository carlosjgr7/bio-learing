package personal.carlosjgr7.biolearing.adapters;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.fragments.ArticulosFragment;
import personal.carlosjgr7.biolearing.models.Articulos;

public class adapterArticulos extends RecyclerView.Adapter<adapterArticulos.ViewHolder> {

    private RealmResults<Articulos> arrayArticulos;
    private OnItemClickListener listener;
    private int layout;
    private Context context;


    public adapterArticulos(RealmResults<Articulos> arrayArticulos, int layout, Context context, OnItemClickListener listener) {
        this.arrayArticulos = arrayArticulos;
        this.listener = listener;
        this.layout = layout;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.layout,null,false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.bind(this.arrayArticulos.get(position),this.listener);

    }

    @Override
    public int getItemCount() {
        return arrayArticulos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView textView_name;
        TextView textView_description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_name = itemView.findViewById(R.id.tv_titleArticulo);
            textView_description = itemView.findViewById(R.id.tv_descriptionArticulo);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(final Articulos articulos, final OnItemClickListener listener) {
            textView_name.setText(articulos.getTituloArticulo());
            textView_description.setText(articulos.getDescripcionArticulo());

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, final View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            menu.setHeaderTitle(arrayArticulos.get(getAdapterPosition()).getTituloArticulo());
            menu.add("Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Articulos> realmResults = realm.where(Articulos.class).equalTo("id",arrayArticulos.get(getAdapterPosition()).getId()).findAll();
                            realmResults.deleteAllFromRealm();
                        }
                    });
                    notifyItemRemoved(getAdapterPosition());
                    ArticulosFragment articulosFragment = FragmentManager.findFragment(view);
                    articulosFragment.mensajeshow();
                    return false;
                }
            });

        }
    }


    public interface OnItemClickListener{
        public void OnClick(Articulos articulo ,int position);
    }

}
