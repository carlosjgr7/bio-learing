package personal.carlosjgr7.biolearing.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.models.Formulario_preguntas;
import personal.carlosjgr7.biolearing.util.Util;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_puntuacion)
    TextView textView_puntuacion;
    @BindView(R.id.first_life)
    ImageView life_one;
    @BindView(R.id.second_life)
    ImageView life_two;
    @BindView(R.id.third_life)
    ImageView life_three;
    @BindView(R.id.quarter_life)
    ImageView life_four;
    @BindView(R.id.fifth_life)
    ImageView life_five;
    @BindView(R.id.tv_pregunta)
    TextView textView_pregunta;
    @BindView(R.id.opcionA)
    TextView textView_opcionA;
    @BindView(R.id.opcionB)
    TextView textView_opcionB;
    @BindView(R.id.opcionC)
    TextView textView_opcionC;
    @BindString(R.string.puntos_bajos)
    String puntos_bajo;
    @BindString(R.string.puntos_medios)
    String puntos_medio;
    @BindString(R.string.puntos_maximos)
    String puntos_altos;


    private int categoria;
    private int gameMode;
    private int cant_preguntas;
    private int nro_pregunta;
    private Button aceptar;
    private ArrayList<Formulario_preguntas> cuestionario;
    private boolean fail=false;
    private int puntuacion;
    private int fallas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        categoria = getIntent().getExtras().getInt("categoria");
        gameMode = getIntent().getExtras().getInt("gameMode");
        LinearLayout layout = findViewById(R.id.layout_vida);

        cargarCuestionario();
        setToolbarValues();
        setDefectoValues();
        setPuntuacionActual(nro_pregunta);
        if (gameMode == Util.quizfast) layout.setVisibility(View.GONE);


        textView_opcionA.setOnClickListener(this);
        textView_opcionB.setOnClickListener(this);
        textView_opcionC.setOnClickListener(this);


    }

    private void setPuntuacionActual(int puntos) {
        textView_puntuacion.setText("Puntuacion Actual: " + puntos + "/" + cant_preguntas);
    }

    private void setDefectoValues() {
        cant_preguntas = cuestionario.size();
        puntuacion=0;
        fallas=0;
        nro_pregunta = 0;
        setNewPregunta(cuestionario.get(nro_pregunta));
    }

    private void setToolbarValues() {
        String title = "";
        String modo_de_juego;

        if (gameMode == Util.quizfast) {
            modo_de_juego = "Quiz Rapido";
        } else {
            modo_de_juego = "Supervivencia";
        }

        if (categoria == Util.SERES_VIVOS) title = "Seres Vivos";
        if (categoria == Util.REINO_ARCHAEA) title = "Reino Archaea";
        if (categoria == Util.REINO_BACTERIA) title = "Reino Bacteria";
        if (categoria == Util.REINO_EUKARYA) title = "Reino Eukarya";

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title + " - " + modo_de_juego);
    }


    public void setNewPregunta(Formulario_preguntas values) {

        textView_pregunta.setText(values.getPregunta());
        textView_opcionA.setText(values.getOpcionA());
        textView_opcionB.setText(values.getOpcionB());
        textView_opcionC.setText(values.getOpcionC());


    }


    private void cargarCuestionario() {

        cuestionario = new ArrayList<Formulario_preguntas>();

        if (categoria == Util.SERES_VIVOS) {
            Util.PreguntasSeresVivosPorDefecto(this.cuestionario);
        }
        if (categoria == Util.REINO_ARCHAEA) {
            Util.PreguntasArchaeaPorDefecto(this.cuestionario);
        }
        if (categoria == Util.REINO_BACTERIA) {
            Util.PreguntasBacteriasPorDefecto(this.cuestionario);
        }
        if (categoria == Util.REINO_EUKARYA) {
            Util.PreguntasEukaryaPorDefecto(this.cuestionario);
        }

    }


    private void verificarRespuesta(String opcion) {
        if (cuestionario.get(nro_pregunta).getRespuestaCorrecta().contentEquals(opcion)) {
            Toast.makeText(this, "Correcto :)", Toast.LENGTH_SHORT).show();
            puntuacion++;
            setPuntuacionActual(puntuacion);

            if ((nro_pregunta + 1) < this.cuestionario.size()) {
                nro_pregunta++;
                setNewPregunta(cuestionario.get(nro_pregunta));
            } else {
                finalizarPartida();
            }
        } else {

            Toast.makeText(this, "Incorrecto :(", Toast.LENGTH_SHORT).show();
            if ((nro_pregunta + 1) < this.cuestionario.size()) {
                fallas++;


                if (gameMode == Util.survivor){
                    VerifyLife();
                    if(fail == false) {
                        nro_pregunta++;
                        setNewPregunta(cuestionario.get(nro_pregunta));
                    }
                }else {
                    nro_pregunta++;
                    setNewPregunta(cuestionario.get(nro_pregunta));
                }

            } else {
                fallas++;
                finalizarPartida();
            }


        }
    }

    private void VerifyLife() {
        if (fallas == 1) {
            life_five.setVisibility(View.GONE);
        }
        if (fallas == 2) {
            life_four.setVisibility(View.GONE);
        }
        if (fallas == 3) {
            life_three.setVisibility(View.GONE);
        }
        if (fallas == 4) {
            life_two.setVisibility(View.GONE);
        }
        if (fallas == 5) {
            life_one.setVisibility(View.GONE);
            finalizarPartida();
            Toast.makeText(this, "Se te agotaron las vidas", Toast.LENGTH_SHORT).show();
        }
        if (fallas > 5) {
            this.fail=true;
        }
    }

    private void finalizarPartida() {
        AlertDialog.Builder builder;
        AlertDialog alertDialog;
        TextView fallas_fin;
        TextView puntuacion_fin;
        TextView calif_fin;

        builder = new AlertDialog.Builder(this);


        View v = LayoutInflater.from(this)
                .inflate(R.layout.resume_game, null);
        builder.setView(v);

        this.aceptar = v.findViewById(R.id.btnOk);
        fallas_fin = v.findViewById(R.id.fallas_fin);
        puntuacion_fin = v.findViewById(R.id.puntuacion_fin);
        calif_fin = v.findViewById(R.id.calificacion_fin);


            if(fallas<5 && fallas>0){
                calif_fin.setText(puntos_medio);
            }if(fallas == 0) {
                calif_fin.setText(puntos_altos);
            }
            if(fallas > 4){
            calif_fin.setText(puntos_bajo);
            }

        puntuacion_fin.setText("Su puntacion final es: "+this.puntuacion+"/"+cuestionario.size());
        fallas_fin.setText("Fallas: "+fallas+" de "+(this.nro_pregunta+1));

        aceptar.setOnClickListener(this);


        alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onClick(View view) {
        if (view == textView_opcionA) {
            verificarRespuesta(cuestionario.get(nro_pregunta).getOpcionA());
        }
        if (view == textView_opcionB) {
            verificarRespuesta(cuestionario.get(nro_pregunta).getOpcionB());
        }
        if (view == textView_opcionC) {
            verificarRespuesta(cuestionario.get(nro_pregunta).getOpcionC());

        }
        if (view == aceptar) {
            exit();
        }


    }

    private void exit() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.exit:
                exit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
