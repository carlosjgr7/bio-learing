package personal.carlosjgr7.biolearing.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.fragments.ChooseOptionFragment;
import personal.carlosjgr7.biolearing.fragments.GameFragment;
import personal.carlosjgr7.biolearing.fragments.InfoFragment;
import personal.carlosjgr7.biolearing.fragments.ArticulosFragment;
import personal.carlosjgr7.biolearing.models.Articulos;
import personal.carlosjgr7.biolearing.util.Util;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        GameFragment.OnFragmentInteractionListener,ChooseOptionFragment.OnFragmentInteractionListener {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindString(R.string.title_play)
    String titleplay;
    @BindString(R.string.surviver_mode)
    String modo_survivor;
    @BindString(R.string.quiz_mode)
    String modo_quiz;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(titleplay);


        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.getMenu().getItem(Util.GAME_FRAGMENT).setChecked(true);



        ChangeViewFragment(new GameFragment());

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        getSupportActionBar().setTitle(menuItem.getTitle());

                switch (menuItem.getItemId()) {
                    case R.id.menu_info:
                        menuItem.setChecked(true);
                        ChangeViewFragment(new InfoFragment());
                        break;
                    case R.id.menu_game:
                        menuItem.setChecked(true);
                        ChangeViewFragment(new GameFragment());

                        break;
                    case R.id.menu_articulos:
                        menuItem.setChecked(true);
                        ChangeViewFragment(new ArticulosFragment());
                        break;
                }

                return false;

        }

    private void ChangeViewFragment(Fragment fragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame_container,fragment)
                .commit();

    }

    @Override
    public void onFragmentInteraction(int option_game) {
        if ((option_game == Util.survivor)) {
            toolbar.setTitle(modo_survivor);
        } else {
            toolbar.setTitle(modo_quiz);
        }

        ChooseOptionFragment choose_option = new ChooseOptionFragment();
        choose_option.Render(option_game);
        ChangeViewFragment(choose_option);
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragmento = manager.findFragmentById(R.id.frame_container);

        if(fragmento instanceof ChooseOptionFragment){
            getSupportActionBar().setTitle(titleplay);
            bottomNavigationView.getMenu().getItem(Util.GAME_FRAGMENT).setChecked(true);
            ChangeViewFragment(new GameFragment());
        }
        if(fragmento instanceof InfoFragment){
            getSupportActionBar().setTitle(titleplay);
            bottomNavigationView.getMenu().getItem(Util.GAME_FRAGMENT).setChecked(true);
            ChangeViewFragment(new GameFragment());
        }
        if(fragmento instanceof ArticulosFragment){
            getSupportActionBar().setTitle(titleplay);
            bottomNavigationView.getMenu().getItem(Util.GAME_FRAGMENT).setChecked(true);
            ChangeViewFragment(new GameFragment());
        }
        if(fragmento instanceof GameFragment){
            ChangeViewFragment(new GameFragment());
        }


    }

    @Override
    public void sendOptionGame(int optionCategory, int optionMode) {
        Intent intent = new Intent(this,GameActivity.class);
        intent.putExtra("categoria",optionCategory);
        intent.putExtra("gameMode",optionMode);
        startActivity(intent);
    }


}
