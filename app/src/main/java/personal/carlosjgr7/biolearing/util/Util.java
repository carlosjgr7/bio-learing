package personal.carlosjgr7.biolearing.util;

import java.util.ArrayList;

import personal.carlosjgr7.biolearing.models.Formulario_preguntas;

public class Util {

    public static final int INFO_FRAGMENT     =0;
    public static final int GAME_FRAGMENT     =1;
    public static final int SETTINGS_FRAGMENT =2;

    public static final int quizfast =0;
    public static final int survivor =1;

    public static final int SERES_VIVOS    = 0;
    public static final int REINO_ARCHAEA  = 1;
    public static final int REINO_BACTERIA = 2;
    public static final int REINO_EUKARYA  = 3;

    public static final int VIDAS  = 5;


    public static void PreguntasSeresVivosPorDefecto(ArrayList<Formulario_preguntas> seresVivos){

        seresVivos.add(new Formulario_preguntas(
                "¿Qué es la biología?",
                "Es la ciencia encargada del estudio de los seres vivos" ,
                "Bios= vida, logos= tratado o razón",
                "Es la ciencia que estudia a los animales",
                "Es la ciencia encargada del estudio de los seres vivos"));

        seresVivos.add(new Formulario_preguntas(
                "¿Cuáles son ejemplos de ramas derivadas de la biología?",
                "Zoología, Odontología y Dermatología" ,
                "Ecología, Minería y Política",
                "Botánica, Genética y Microbiología",
                "Botánica, Genética y Microbiología"));

        seresVivos.add(new Formulario_preguntas(
                "¿Qué estudia la Taxonomía?",
                "Clasificación de los seres vivos" ,
                "Células",
                "Evolución",
                "Clasificación de los seres vivos"));

        seresVivos.add(new Formulario_preguntas(
                "¿Qué es la Histología?",
                "Herencia" ,
                "Restos Fosiles",
                "Tejidos celulares",
                "Tejidos celulares"));

        seresVivos.add(new Formulario_preguntas(
                "¿Cuál es un ejemplo de características de los seres vivos?",
                "Metabolismo" ,
                "Pluricelular",
                "Heterotrófo",
                "Metabolismo"));

        seresVivos.add(new Formulario_preguntas(
                "¿A qué nos referimos con “Irritabilidad”?",
                "Aumentar la talla y peso de los organismos" ,
                "Crear nuevos organismos / dejar descendencia",
                "Respuestas internas de los seres vivos a los estímulos del ambiente",
                "Respuestas internas de los seres vivos a los estímulos del ambiente"));

        seresVivos.add(new Formulario_preguntas(
                "¿A qué nos referimos con “Crecimiento”?",
                "Capacidad de desplazamiento de los organismos" ,
                "Aumento de talla y peso de los organismos",
                "Modificaciones frente a estímulos del medio interno y externo que permiten a la especie sobrevivir al cambio constante",
                "Aumento de talla y peso de los organismos"));

        seresVivos.add(new Formulario_preguntas(
                "¿Qué es Complejidad?",
                "Intercambio constante de materia y energía que ocurre en los organismos" ,
                "Aprovechamiento y transformación de materia y energía",
                "Respuestas internas de los seres vivos a los estímulos del ambiente",
                "Intercambio constante de materia y energía que ocurre en los organismos"));

        seresVivos.add(new Formulario_preguntas(
                "¿Qué es Movimiento?",
                "Capacidad de desplazamiento de los organismos" ,
                "Proceso por el que se conservan las condiciones químicas y físicas internas",
                "Aumento de talla y peso",
                "Capacidad de desplazamiento de los organismos"));

        seresVivos.add(new Formulario_preguntas(
                "¿Qué es Metabolismo?",
                "Capacidad de desplazamiento de los organismos" ,
                "Aprovechamiento y transformación de materia y energía",
                "Proceso por el que se conservan las condiciones químicas y físicas internas",
                "Aprovechamiento y transformación de materia y energía"));
    }


    public static void PreguntasArchaeaPorDefecto(ArrayList<Formulario_preguntas> archaea){

        archaea.add(new Formulario_preguntas(
                "¿Quién propuso el reino Protista?",
                "Ernst Haecke" ,
                "Herbert Copeland",
                " Robert Whittaker",
                "Ernst Haecke"));

        archaea.add(new Formulario_preguntas(
                "¿En qué año Herbert Copeland propuso el reino Monera?",
                "1886" ,
                "1969",
                "1938",
                "1938"));

        archaea.add(new Formulario_preguntas(
                "¿Quién propuso la clasificación taxonómica de cinco reinos (Monera, Protista, Plantae, Animalia y Fungi)?",
                "Ernst Haeckel" ,
                "Herbert Copeland",
                "Robert Whittaker",
                "Robert Whittaker"));

        archaea.add(new Formulario_preguntas(
                "¿Qué significa “extremófilas”?",
                "Organismos de condiciones extremas" ,
                "Organismos anaerobios",
                "Organismos de temperaturas extremas",
                "Organismos de condiciones extremas"));

        archaea.add(new Formulario_preguntas(
                "¿Cuáles son los tres dominios que planteo Carl Woese?",
                "Monera, Protista y Fungi" ,
                "Bacteria, Arquea y Eukarya",
                "Anaerobias, Anaerobias facultativas y Aerobias",
                "Bacteria, Arquea y Eukarya"));

        archaea.add(new Formulario_preguntas(
                "¿Se cree que la diversidad surgió de un solo microorganismo ¿De quién hablamos?",
                "Lucy" ,
                "Big Bang",
                "LUCA",
                "LUCA"));

        archaea.add(new Formulario_preguntas(
                "¿Qué significa “LUCA”?",
                "Last Universal Common Ancesor" ,
                "Último Ancestro Universal Común",
                "Ambas",
                "Ambas"));

        archaea.add(new Formulario_preguntas(
                "¿Son un tipo de Arquenobacteria que incluye las que producen metano y las que toleran la sal?",
                "Euryarchaeotas" ,
                "Crenarchaeotas",
                "Korarchaeotas",
                "Euryarchaeotas"));

        archaea.add(new Formulario_preguntas(
                "¿Son un tipo de Arquenobacteria del que se sabe muy poco?",
                "Euryarchaeotas" ,
                "Crenarchaeotas",
                "Korarchaeotas",
                "Korarchaeotas"));

        archaea.add(new Formulario_preguntas(
                "¿Son un tipo de Arquenobacteria que soportan altas temperaturas y acidez?",
                "Euryarchaeota" ,
                "Crenarchaeotas",
                "Korarchaeotas",
                "Crenarchaeotas"));
        }


    public static void PreguntasBacteriasPorDefecto(ArrayList<Formulario_preguntas> bacterias) {

        bacterias.add(new Formulario_preguntas(
                "¿Qué son las bacterias?",
                "Animales chiquitos",
                "Organismos unicelulares",
                "Organismos pluricelulares",
                "Organismos unicelulares"));

        bacterias.add(new Formulario_preguntas(
                "¿Cuáles son características de las bacterias?",
                "Tienen núcleo definido, están en todos los ambientes y son metazoarios",
                "Miden entre 1 y 10 micras, tienen orgánulos y su reproducción es sexual",
                "Contienen uno o dos cromosomas circulares, Gram  negativas y tienen muchos ribosomas",
                "Contienen uno o dos cromosomas circulares, Gram  negativas y tienen muchos ribosomas"));

        bacterias.add(new Formulario_preguntas(
                "¿Qué estructura tienen las bacterias?",
                "Eucariota",
                "Animalia",
                "Procariota",
                "Procariota"));

        bacterias.add(new Formulario_preguntas(
                "¿Cómo se reproducen las bacterias?",
                "Fisión binaria",
                "Reproducción sexual",
                "Ambas",
                "Fisión binaria"));

        bacterias.add(new Formulario_preguntas(
                "¿Cómo se denominan a las bacterias Gram negativas?",
                "A las que se tiñen de rojo con la tinción",
                "A las que no se tiñen con la tinción",
                "A las que se tiñen de azul obscuro/ violeta",
                "A las que se tiñen de azul obscuro/ violeta"));

        bacterias.add(new Formulario_preguntas(
                "¿Qué son los plásmidos?",
                "Pequeños lazos de ADN distribuidos en el citoplasma",
                "Plasma",
                "Cromosomas",
                "Pequeños lazos de ADN distribuidos en el citoplasma"));

        bacterias.add(new Formulario_preguntas(
                "Los genes bacterianos se encuentran organizados en un sistema conocido cómo:",
                "Plásmidos",
                "Operón",
                "Tinción de Gram",
                "Operón"));

        bacterias.add(new Formulario_preguntas(
                "Son autótrofas, obtienen energía de la oxidación de compuestos inorgánicos:",
                "Fotosintetizadoras",
                "Heterótrofas",
                "Quimiosintetizadoras",
                "Quimiosintetizadoras"));

        bacterias.add(new Formulario_preguntas(
                "Convierten la luz en energía química (carbohidratos):",
                "Fotosintetizadoras",
                "Heterótrofas",
                "Quimiosintetizadoras",
                "Fotosintetizadoras"));

        bacterias.add(new Formulario_preguntas(
                "Obtienen su energía de materia orgánica elaborada por otros organismos. Se alimentan de materia muerta o en descomposición y reciclan los nutrientes:",
                "Fotosintetizadoras",
                "Heterótrofas",
                "Quimisontetizadoras",
                "Heterótrofas"));
        }

    public static void PreguntasEukaryaPorDefecto(ArrayList<Formulario_preguntas> bacterias) {

        bacterias.add(new Formulario_preguntas(
                "¿Qué organismos incluye  el dominio Eukarya?",
                "Incluye los organismos formados por células con núcleo verdadero",
                "Incluye a los organismos formados por células sin núcleo",
                "Incluye a los organismos que son vertebrados",
                "Incluye los organismos formados por células con núcleo verdadero"));

        bacterias.add(new Formulario_preguntas(
                "Es el primer reino con organismos eucariotas, sus células posen núcleo verdadero y orgánulos, lo cual implica una división y áreas específicas dedicadas a funciones determinadas",
                "Reino Monera",
                "Reino Fungi",
                "Reino Protista",
                "Reino Protista"));

        bacterias.add(new Formulario_preguntas(
                "¿Cuáles son características del Reino Protista?",
                "Capacidad de locomoción o desplazamiento",
                "Se encuentran en tierra árida",
                "Ambos",
                "Capacidad de locomoción o desplazamiento"));

        bacterias.add(new Formulario_preguntas(
                "¿Qué son las diatomeas?",
                "Una especie de pescados",
                "Organismos fotosintetizadores, parte del fitoplancton",
                "Organismos parásitos de agua marina",
                "Organismos fotosintetizadores, parte del fitoplancton"));

        bacterias.add(new Formulario_preguntas(
                "¿Con qué otro nombre se le conoce a los pseudópodos?",
                "Patitas",
                "Pies de mentira",
                "Falsos pies",
                "Falsos pies"));

        bacterias.add(new Formulario_preguntas(
                "¿Para qué sirven los pseudópodos?",
                "Para capturar alimento y digerirlo con enzimas",
                "Para percibir vibraciones",
                "Para huir de depredadores",
                "Para capturar alimento y digerirlo con enzimas"));

        bacterias.add(new Formulario_preguntas(
                "Se desplazan por pseudópodos:",
                "Flagelados",
                "Algas",
                "Sarcodinos",
                "Sarcodinos"));

        bacterias.add(new Formulario_preguntas(
                "Causa infección intestinal o hepática por ingesta de alimentos contaminados",
                "Foraminíferos",
                "Ameba",
                "Entamoeba histolitica",
                "Ameba"));

        bacterias.add(new Formulario_preguntas(
                "Produce la disentería, enfermedad de los países tropicales que ocasiona diarreas intensas",
                "Foraminíferos ",
                "Ameba ",
                "Entamoeba histolitica",
                "Entamoeba histolitica"));

        bacterias.add(new Formulario_preguntas(
                "Se trasladan y capturan el alimento por medio de cilios. Únicos organismos con dos núcleos, uno relacionado con la reproducción y otro con la alimentación",
                "Esporozos ",
                "Flagelados ",
                "Ciliados",
                "Ciliados"));


    }


    }
