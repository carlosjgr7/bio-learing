package personal.carlosjgr7.biolearing.util;

import android.app.Application;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import personal.carlosjgr7.biolearing.models.Articulos;

public class ConfigRealm extends Application {


    public static AtomicInteger articulosID = new AtomicInteger();


    @Override
    public void onCreate() {
        super.onCreate();
        setupConfig();
        Realm realm = Realm.getDefaultInstance();

        articulosID = getIdByTable(realm, Articulos.class);
        realm.close();

    }

    private<T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyclass) {
        RealmResults<T> results = realm.where(anyclass).findAll();
        return (results.size()>0)?new AtomicInteger(results.max("id").intValue()):new AtomicInteger();
    }

    private void setupConfig() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

}


