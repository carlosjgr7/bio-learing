package personal.carlosjgr7.biolearing.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import personal.carlosjgr7.biolearing.util.ConfigRealm;

public class Articulos extends RealmObject {

    @PrimaryKey
    private int id;
    private String tituloArticulo;
    private String descripcionArticulo;

    public Articulos() {
    }

    public Articulos(String tituloArticulo, String descripcionArticulo) {
        this.id = ConfigRealm.articulosID.incrementAndGet();
        this.tituloArticulo = tituloArticulo;
        this.descripcionArticulo = descripcionArticulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTituloArticulo() {
        return tituloArticulo;
    }

    public void setTituloArticulo(String tituloArticulo) {
        this.tituloArticulo = tituloArticulo;
    }

    public String getDescripcionArticulo() {
        return descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }
}
