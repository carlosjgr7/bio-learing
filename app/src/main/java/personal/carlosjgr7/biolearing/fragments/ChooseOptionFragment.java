package personal.carlosjgr7.biolearing.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.util.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseOptionFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.card_seres_vivos)
    CardView card_seres_vivos;
    @BindView(R.id.card_archea)
    CardView card_archea;
    @BindView(R.id.card_bacteria)
    CardView card_bacteria;
    @BindView(R.id.card_eukarya)
    CardView card_eukarya;

    OnFragmentInteractionListener mListener;


    private int optionMode;

    public ChooseOptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_option, container, false);
        ButterKnife.bind(this, view);

        card_seres_vivos.setOnClickListener(this);
        card_archea.setOnClickListener(this);
        card_bacteria.setOnClickListener(this);
        card_eukarya.setOnClickListener(this);

        return view;
    }


    public void Render(int option_game) {
        this.optionMode = option_game;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onClick(View view) {
        if(view.equals(card_seres_vivos)){
            mListener.sendOptionGame(Util.SERES_VIVOS,optionMode);
        }
        if(view.equals(card_archea)){
            mListener.sendOptionGame(Util.REINO_ARCHAEA,optionMode);
        }
        if(view.equals(card_bacteria)){
            mListener.sendOptionGame(Util.REINO_BACTERIA,optionMode);
        }
        if(view.equals(card_eukarya)){
            mListener.sendOptionGame(Util.REINO_EUKARYA,optionMode);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void sendOptionGame(int optionCategory, int optionMode);
    }

}
