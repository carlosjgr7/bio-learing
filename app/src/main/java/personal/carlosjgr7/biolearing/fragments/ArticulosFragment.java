package personal.carlosjgr7.biolearing.fragments;


import android.app.AlertDialog;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.adapters.adapterArticulos;
import personal.carlosjgr7.biolearing.models.Articulos;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArticulosFragment extends Fragment implements View.OnClickListener, RealmChangeListener<RealmResults<Articulos>> {

    private RecyclerView recyclerArticulos;
    private RecyclerView.LayoutManager layoutManager;
    private adapterArticulos adapterArticulos;
    private Realm realm;
    private RealmResults<Articulos> realmArticulos;
    private FloatingActionButton fab;
    private Button guardar;
    private Button cancelar;
    private TextView tv_title_new;
    private TextView tv_description_new;
    private int sizeOfArticulos;
    private AlertDialog alertDialog;
    private OnFragmentInteractionListener listener;
    private CardView cardView;
    TextView textViewmensaje;


    public ArticulosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_articulo, container, false);

        layoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);

        recyclerArticulos = view.findViewById(R.id.recycler_arrticulos);
        fab = view.findViewById(R.id.floatingActionButtonArticulos);
        recyclerArticulos.setHasFixedSize(true);
        recyclerArticulos.setItemAnimator(new DefaultItemAnimator());
        recyclerArticulos.setLayoutManager(layoutManager);
        textViewmensaje = view.findViewById(R.id.view_articulo_mensaje);


        realm = Realm.getDefaultInstance();

        realmArticulos = realm.where(Articulos.class).findAll();


        this.sizeOfArticulos = realmArticulos.size();
        mensajeshow();
       adapterArticulos = new adapterArticulos(realmArticulos, R.layout.view_articulos, view.getContext(), new adapterArticulos.OnItemClickListener() {
           @Override
           public void OnClick(Articulos articulo, int position) {
           }
       });
       recyclerArticulos.setAdapter(adapterArticulos);

        fab.setOnClickListener(this);

        return view;
    }

    public  void mensajeshow() {
        if(realmArticulos.size()<1){
            textViewmensaje.setVisibility(View.VISIBLE);
        }
        else {
            textViewmensaje.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View view) {
        if (view.equals(fab)){
        
            newArticulo();
        }
        if(view.equals(guardar)){
            Boolean findNull = findNLL();
            if(findNull == false){
                Toast.makeText(getContext(), "Datos imcompletos para crear un cliente",
                        Toast.LENGTH_SHORT).show();

            }else {
                Articulos nuevo_articulo = new Articulos(tv_title_new.getText().toString(),tv_description_new.getText().toString());
                aggToDataBase(nuevo_articulo);
                this.alertDialog.cancel();
            }

        }

        if(view.equals(cancelar)){
            alertDialog.cancel();

        }
        
        
        
        
    }

    private void aggToDataBase(final Articulos nuevo_articulo) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(nuevo_articulo);
            }
        });
        adapterArticulos.notifyDataSetChanged();
        recyclerArticulos.scrollToPosition(realmArticulos.size()-1);
        mensajeshow();
    }



    private Boolean findNLL() {
        if((tv_title_new.getText().toString().isEmpty())||(tv_description_new.getText().toString().isEmpty())){
            return false;
        }
        return true;
    }

    private void newArticulo() {

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Nuevo Articulo");

        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.create_new_articulo,null);
        builder.setView(v);

        this.tv_title_new = v.findViewById(R.id.title_new);
        this.tv_description_new = v.findViewById(R.id.description_new);
        this.cancelar = v.findViewById(R.id.cancel_new);
        this.guardar = v.findViewById(R.id.save_new);


        guardar.setOnClickListener(this);
        cancelar.setOnClickListener(this);

        this.alertDialog = builder.create();
        alertDialog.show();

    }


    @Override
    public void onChange(RealmResults<Articulos> articulos) {
        if (this.sizeOfArticulos<realmArticulos.size()){
            adapterArticulos.notifyDataSetChanged();
            recyclerArticulos.scrollToPosition(realmArticulos.size()-1);
            sizeOfArticulos = realmArticulos.size();

        }
        sizeOfArticulos = realmArticulos.size();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void sendArticulo(Articulos articulo);
    }


}
