package personal.carlosjgr7.biolearing.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;


import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import personal.carlosjgr7.biolearing.R;
import personal.carlosjgr7.biolearing.util.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class GameFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.card_quiz_rapido)
    CardView cardView_quiz;
    @BindView(R.id.card_supervivencia)
    CardView cardView_survivor;



    private OnFragmentInteractionListener mListener;
    private View view;



    public GameFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_game, container, false);
        ButterKnife.bind(this, view);
        cardView_quiz.setOnClickListener(this);
        cardView_survivor.setOnClickListener(this);
        this.view = view;

        return view;
    }


    @Override
    public void onClick(View view) {
        if (view == cardView_quiz) {
            mListener.onFragmentInteraction(Util.quizfast);
        }
        if (view == cardView_survivor) {
            mListener.onFragmentInteraction(Util.survivor);
        }

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int option_game);
    }

}
